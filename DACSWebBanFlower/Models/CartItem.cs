﻿namespace DACSWebBanFlower.Models
{
    public class CartItem
    {
        public string MaSp { get; set; } // Tên thuộc tính đã được sửa đổi để tuân thủ quy tắc PascalCase
        public string TenSp { get; set; }
        public decimal? Price { get; set; }
        public int SoLuong { get; set; }
        public decimal? ThanhTien => SoLuong * Price; // Sửa kiểu dữ liệu của ThanhTien để phù hợp với Price
    }
}
