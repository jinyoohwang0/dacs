﻿namespace DACSWebBanFlower.Models.RegisterModels
{
    public class RegisterModel
    {
        public string Id { get; set; }
        public string Password { get; set; }
        public string TenKhachHang { get; set; }
        public string SoDienThoai { get; set; }
    }
}
