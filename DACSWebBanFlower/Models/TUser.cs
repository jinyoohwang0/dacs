﻿namespace DACSWebBanFlower.Models
{
    public class TUser
    {
        public string Id { get; set; } = null!;

        public string Password { get; set; } = null!;

        public string? TenKhachHang { get; set; } = null!;

        public int? SoDienThoai { get; set; }
        public byte? LoaiUser { get; set; }

        public virtual ICollection<TKhachHang> TKhachHangs { get; set; } = new List<TKhachHang>();

        public virtual ICollection<TNhanVien> TNhanViens { get; set; } = new List<TNhanVien>();
    }
}
