﻿namespace DACSWebBanFlower.Models
{
    public class TDanhMucSp
    {
        public string Id { get; set; } = null!;

        public string? TenSp { get; set; }

        public string? ChatLieuId { get; set; }

        public string? HangSxId { get; set; }

        public string? NuocSxId { get; set; }

        public string? Website { get; set; }

        public string? GioiThieuSp { get; set; }

        public string? MaLoai { get; set; }

        public string? AnhDaiDien { get; set; }

        public decimal? GiaNhoNhat { get; set; }

        public decimal? GiaLonNhat { get; set; }

        public  TChatLieu? TChatLieu { get; set; }

        public  THangSx? THangSx { get; set; }

        public  TLoaiSp? TLoaiSp { get; set; }

        public  TKhuVuc? TKhuVuc { get; set; }

        public virtual ICollection<TAnhSp> TAnhSps { get; set; } = new List<TAnhSp>();

        public virtual ICollection<TChiTietSanPham> TChiTietSanPhams { get; set; } = new List<TChiTietSanPham>();
    }
}
