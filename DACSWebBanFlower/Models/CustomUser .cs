﻿    using Microsoft.AspNetCore.Identity;

    namespace DACSWebBanFlower.Models
    {
        public class CustomUser : IdentityUser
        {
        public string? TenKhachHang { get; set; }
        public byte? LoaiUser { get; set; }
    }
    }
