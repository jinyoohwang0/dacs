﻿using DACSWebBanFlower.Models;

namespace DACSWebBanFlower.Models
{
    public class ShoppingCart
    {
        public List<CartItem> Items { get; set; } = new List<CartItem>();
        public void AddItem(CartItem item)
        {
            var existingItem = Items.FirstOrDefault(i => i.MaSp ==
            item.MaSp);
            if (existingItem != null)
            {
                existingItem.SoLuong += item.SoLuong;
            }
            else
            {
                Items.Add(item);
            }
        }
        public void RemoveItem(string productId)
        {
            var itemToRemove = Items.FirstOrDefault(i => i.MaSp == productId);
            if (itemToRemove != null)
            {
                Items.Remove(itemToRemove);
            }
        }
        // Các phương thức khác...
    }
}