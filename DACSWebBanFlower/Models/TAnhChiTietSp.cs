﻿namespace DACSWebBanFlower.Models
{
    public class TAnhChiTietSp
    {
        public string Id { get; set; } = null!;

        public required string TenFileAnh { get; set; } 

        public short? ViTri { get; set; }

        public virtual TChiTietSanPham? ChiTietSanPhamId { get; set; } 
    }
}
