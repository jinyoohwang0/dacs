﻿namespace DACSWebBanFlower.Models
{
    public class TKichThuoc
    {
        public string Id { get; set; } = null!;

        public string? KichThuoc { get; set; }

        public virtual ICollection<TChiTietSanPham> TChiTietSanPhams { get; set; } = new List<TChiTietSanPham>();
    }
}
