﻿namespace DACSWebBanFlower.Models
{
    public class THoaDonBan
    {
        public string Id { get; set; } = null!;

        public string UserId { get; set; } = null!;

        public DateTime? NgayHoaDon { get; set; }

        public decimal? TongTienHd { get; set; }

        public string? DiaChiDatHang { get; set; }

        public string? GhiChu { get; set; }
            
        public TUser? User { get; set; }
        public virtual ICollection<TChiTietHdb> TChiTietHdbs { get; set; } = new List<TChiTietHdb>();
    }
}
