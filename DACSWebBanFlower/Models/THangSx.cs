﻿namespace DACSWebBanFlower.Models
{
    public class THangSx
    {
        public string Id { get; set; } = null!;

        public string? HangSx { get; set; }

        public string? MaNuocThuongHieu { get; set; }

        public virtual ICollection<TDanhMucSp> TDanhMucSps { get; set; } = new List<TDanhMucSp>();
    }
}
