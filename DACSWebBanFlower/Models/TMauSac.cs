﻿namespace DACSWebBanFlower.Models
{
    public class TMauSac
    {
        public string Id { get; set; } = null!;

        public string? TenMauSac { get; set; }

        public virtual ICollection<TChiTietSanPham> TChiTietSanPhams { get; set; } = new List<TChiTietSanPham>();
    }
}
