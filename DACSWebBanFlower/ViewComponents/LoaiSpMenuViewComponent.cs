﻿using DACSWebBanFlower.Models;
using Microsoft.AspNetCore.Mvc;
using DACSWebBanFlower.Repositories;

namespace DACSWebBanFlower.ViewComponents
{
    public class LoaiSpMenuViewComponent: ViewComponent
    {
        private readonly ILoaiSpRepository _loaiSp;

        public LoaiSpMenuViewComponent(ILoaiSpRepository loaiSpRepository)
        {
            _loaiSp = loaiSpRepository;
        }
        public IViewComponentResult Invoke()
        {
            var loaiSp = _loaiSp.GetAllLoaiSp().OrderBy(X => X.Loai);
            return View(loaiSp);
        }
    }
}
