﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DACSWebBanFlower.Migrations
{
    /// <inheritdoc />
    public partial class newHoaDOn : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_THoaDonBans_AspNetUsers_UserId",
                table: "THoaDonBans");

            migrationBuilder.DropColumn(
                name: "GiamGiaHd",
                table: "THoaDonBans");

            migrationBuilder.DropColumn(
                name: "KhachHangId",
                table: "THoaDonBans");

            migrationBuilder.DropColumn(
                name: "MaSoThue",
                table: "THoaDonBans");

            migrationBuilder.DropColumn(
                name: "NhanVienId",
                table: "THoaDonBans");

            migrationBuilder.DropColumn(
                name: "PhuongThucThanhToan",
                table: "THoaDonBans");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "THoaDonBans",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_THoaDonBans_TUsers_UserId",
                table: "THoaDonBans",
                column: "UserId",
                principalTable: "TUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_THoaDonBans_TUsers_UserId",
                table: "THoaDonBans");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "THoaDonBans",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddColumn<double>(
                name: "GiamGiaHd",
                table: "THoaDonBans",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "KhachHangId",
                table: "THoaDonBans",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MaSoThue",
                table: "THoaDonBans",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NhanVienId",
                table: "THoaDonBans",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "PhuongThucThanhToan",
                table: "THoaDonBans",
                type: "tinyint",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_THoaDonBans_AspNetUsers_UserId",
                table: "THoaDonBans",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");
        }
    }
}
