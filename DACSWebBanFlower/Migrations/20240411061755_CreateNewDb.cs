﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DACSWebBanFlower.Migrations
{
    /// <inheritdoc />
    public partial class CreateNewDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TChatLieus",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ChatLieu = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TChatLieus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "THangSxes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    HangSx = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MaNuocThuongHieu = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_THangSxes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TKhuVucs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TenKhuVuc = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TKhuVucs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TKichThuocs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    KichThuoc = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TKichThuocs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TLoaiSps",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Loai = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TLoaiSps", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TMauSacs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TenMauSac = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMauSacs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TenKhachHang = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SoDienThoai = table.Column<int>(type: "int", nullable: true),
                    LoaiUser = table.Column<byte>(type: "tinyint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TDanhMucSps",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TenSp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ChatLieuId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HangSxId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NuocSxId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Website = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GioiThieuSp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MaLoai = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AnhDaiDien = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GiaNhoNhat = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    GiaLonNhat = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    TChatLieuId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    THangSxId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TLoaiSpId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TKhuVucId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TDanhMucSps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TDanhMucSps_TChatLieus_TChatLieuId",
                        column: x => x.TChatLieuId,
                        principalTable: "TChatLieus",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TDanhMucSps_THangSxes_THangSxId",
                        column: x => x.THangSxId,
                        principalTable: "THangSxes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TDanhMucSps_TKhuVucs_TKhuVucId",
                        column: x => x.TKhuVucId,
                        principalTable: "TKhuVucs",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TDanhMucSps_TLoaiSps_TLoaiSpId",
                        column: x => x.TLoaiSpId,
                        principalTable: "TLoaiSps",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TKhachHangs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenKhachHang = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NgaySinh = table.Column<DateOnly>(type: "date", nullable: true),
                    SoDienThoai = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DiaChi = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LoaiKhachHang = table.Column<byte>(type: "tinyint", nullable: true),
                    AnhDaiDien = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TUserId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TKhachHangs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TKhachHangs_TUsers_TUserId",
                        column: x => x.TUserId,
                        principalTable: "TUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TNhanViens",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenNhanVien = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NgaySinh = table.Column<DateOnly>(type: "date", nullable: true),
                    SoDienThoai1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SoDienThoai2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DiaChi = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ChucVu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AnhDaiDien = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TUserId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TNhanViens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TNhanViens_TUsers_TUserId",
                        column: x => x.TUserId,
                        principalTable: "TUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TAnhSps",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TenFileAnh = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ViTri = table.Column<short>(type: "smallint", nullable: true),
                    DanhMucSpIdId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TAnhSps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TAnhSps_TDanhMucSps_DanhMucSpIdId",
                        column: x => x.DanhMucSpIdId,
                        principalTable: "TDanhMucSps",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TChiTietSanPhams",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    DanhMucSpId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    KichThuocId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MauSacId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AnhDaiDien = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Video = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DonGiaBan = table.Column<double>(type: "float", nullable: true),
                    GiamGia = table.Column<double>(type: "float", nullable: true),
                    Slton = table.Column<int>(type: "int", nullable: true),
                    TKichThuocId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TMauSacId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TDanhMucSpId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TChiTietSanPhams", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TChiTietSanPhams_TDanhMucSps_TDanhMucSpId",
                        column: x => x.TDanhMucSpId,
                        principalTable: "TDanhMucSps",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TChiTietSanPhams_TKichThuocs_TKichThuocId",
                        column: x => x.TKichThuocId,
                        principalTable: "TKichThuocs",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TChiTietSanPhams_TMauSacs_TMauSacId",
                        column: x => x.TMauSacId,
                        principalTable: "TMauSacs",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "THoaDonBans",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    NgayHoaDon = table.Column<DateTime>(type: "datetime2", nullable: true),
                    KhachHangId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NhanVienId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TongTienHd = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    GiamGiaHd = table.Column<double>(type: "float", nullable: true),
                    PhuongThucThanhToan = table.Column<byte>(type: "tinyint", nullable: true),
                    MaSoThue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ThongTinThue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TKhachHangId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TNhanVienId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_THoaDonBans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_THoaDonBans_TKhachHangs_TKhachHangId",
                        column: x => x.TKhachHangId,
                        principalTable: "TKhachHangs",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_THoaDonBans_TNhanViens_TNhanVienId",
                        column: x => x.TNhanVienId,
                        principalTable: "TNhanViens",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TAnhChiTietSps",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TenFileAnh = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ViTri = table.Column<short>(type: "smallint", nullable: true),
                    ChiTietSanPhamIdId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TAnhChiTietSps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TAnhChiTietSps_TChiTietSanPhams_ChiTietSanPhamIdId",
                        column: x => x.ChiTietSanPhamIdId,
                        principalTable: "TChiTietSanPhams",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TChiTietHdbs",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ChiTietSanPhamId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SoLuongBan = table.Column<int>(type: "int", nullable: true),
                    DonGiaBan = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    GiamGia = table.Column<double>(type: "float", nullable: true),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TChiTietSanPhamId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    THoaDonBanId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TChiTietHdbs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TChiTietHdbs_TChiTietSanPhams_TChiTietSanPhamId",
                        column: x => x.TChiTietSanPhamId,
                        principalTable: "TChiTietSanPhams",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TChiTietHdbs_THoaDonBans_THoaDonBanId",
                        column: x => x.THoaDonBanId,
                        principalTable: "THoaDonBans",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_TAnhChiTietSps_ChiTietSanPhamIdId",
                table: "TAnhChiTietSps",
                column: "ChiTietSanPhamIdId");

            migrationBuilder.CreateIndex(
                name: "IX_TAnhSps_DanhMucSpIdId",
                table: "TAnhSps",
                column: "DanhMucSpIdId");

            migrationBuilder.CreateIndex(
                name: "IX_TChiTietHdbs_TChiTietSanPhamId",
                table: "TChiTietHdbs",
                column: "TChiTietSanPhamId");

            migrationBuilder.CreateIndex(
                name: "IX_TChiTietHdbs_THoaDonBanId",
                table: "TChiTietHdbs",
                column: "THoaDonBanId");

            migrationBuilder.CreateIndex(
                name: "IX_TChiTietSanPhams_TDanhMucSpId",
                table: "TChiTietSanPhams",
                column: "TDanhMucSpId");

            migrationBuilder.CreateIndex(
                name: "IX_TChiTietSanPhams_TKichThuocId",
                table: "TChiTietSanPhams",
                column: "TKichThuocId");

            migrationBuilder.CreateIndex(
                name: "IX_TChiTietSanPhams_TMauSacId",
                table: "TChiTietSanPhams",
                column: "TMauSacId");

            migrationBuilder.CreateIndex(
                name: "IX_TDanhMucSps_TChatLieuId",
                table: "TDanhMucSps",
                column: "TChatLieuId");

            migrationBuilder.CreateIndex(
                name: "IX_TDanhMucSps_THangSxId",
                table: "TDanhMucSps",
                column: "THangSxId");

            migrationBuilder.CreateIndex(
                name: "IX_TDanhMucSps_TKhuVucId",
                table: "TDanhMucSps",
                column: "TKhuVucId");

            migrationBuilder.CreateIndex(
                name: "IX_TDanhMucSps_TLoaiSpId",
                table: "TDanhMucSps",
                column: "TLoaiSpId");

            migrationBuilder.CreateIndex(
                name: "IX_THoaDonBans_TKhachHangId",
                table: "THoaDonBans",
                column: "TKhachHangId");

            migrationBuilder.CreateIndex(
                name: "IX_THoaDonBans_TNhanVienId",
                table: "THoaDonBans",
                column: "TNhanVienId");

            migrationBuilder.CreateIndex(
                name: "IX_TKhachHangs_TUserId",
                table: "TKhachHangs",
                column: "TUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TNhanViens_TUserId",
                table: "TNhanViens",
                column: "TUserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TAnhChiTietSps");

            migrationBuilder.DropTable(
                name: "TAnhSps");

            migrationBuilder.DropTable(
                name: "TChiTietHdbs");

            migrationBuilder.DropTable(
                name: "TChiTietSanPhams");

            migrationBuilder.DropTable(
                name: "THoaDonBans");

            migrationBuilder.DropTable(
                name: "TDanhMucSps");

            migrationBuilder.DropTable(
                name: "TKichThuocs");

            migrationBuilder.DropTable(
                name: "TMauSacs");

            migrationBuilder.DropTable(
                name: "TKhachHangs");

            migrationBuilder.DropTable(
                name: "TNhanViens");

            migrationBuilder.DropTable(
                name: "TChatLieus");

            migrationBuilder.DropTable(
                name: "THangSxes");

            migrationBuilder.DropTable(
                name: "TKhuVucs");

            migrationBuilder.DropTable(
                name: "TLoaiSps");

            migrationBuilder.DropTable(
                name: "TUsers");
        }
    }
}
