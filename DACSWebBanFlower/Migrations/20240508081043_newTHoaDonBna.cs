﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DACSWebBanFlower.Migrations
{
    /// <inheritdoc />
    public partial class newTHoaDonBna : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DanhMucSpId",
                table: "TChiTietHdbs",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TChiTietHdbs_DanhMucSpId",
                table: "TChiTietHdbs",
                column: "DanhMucSpId");

            migrationBuilder.AddForeignKey(
                name: "FK_TChiTietHdbs_TDanhMucSps_DanhMucSpId",
                table: "TChiTietHdbs",
                column: "DanhMucSpId",
                principalTable: "TDanhMucSps",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TChiTietHdbs_TDanhMucSps_DanhMucSpId",
                table: "TChiTietHdbs");

            migrationBuilder.DropIndex(
                name: "IX_TChiTietHdbs_DanhMucSpId",
                table: "TChiTietHdbs");

            migrationBuilder.DropColumn(
                name: "DanhMucSpId",
                table: "TChiTietHdbs");
        }
    }
}
