﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DACSWebBanFlower.Migrations
{
    /// <inheritdoc />
    public partial class CreateDiaChiDatHang : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ThongTinThue",
                table: "THoaDonBans",
                newName: "DiaChiDatHang");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DiaChiDatHang",
                table: "THoaDonBans",
                newName: "ThongTinThue");
        }
    }
}
