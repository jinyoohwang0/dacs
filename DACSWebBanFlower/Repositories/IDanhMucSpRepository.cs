﻿using DACSWebBanFlower.Models;

namespace DACSWebBanFlower.Repositories
{
    public interface IDanhMucSpRepository
    {
        Task<IEnumerable<TDanhMucSp>> GetAllAsync();
        Task<TDanhMucSp> GetByIdAsync(string id);
        Task AddAsync(TDanhMucSp danhMuc);
        Task UpdateAsync(TDanhMucSp danhMuc);
        Task DeleteAsync(string id);
    }
}
