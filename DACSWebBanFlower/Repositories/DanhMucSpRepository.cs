﻿using DACSWebBanFlower.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DACSWebBanFlower.Repositories
{
    public class DanhMucSpRepository : IDanhMucSpRepository
    {
        private readonly QlbanFlowerContext _context;

        public DanhMucSpRepository(QlbanFlowerContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<TDanhMucSp>> GetAllAsync()
        {
            return await _context.TDanhMucSps.ToListAsync();
        }

        public async Task<TDanhMucSp> GetByIdAsync(string id)
        {
            return await _context.TDanhMucSps.FindAsync(id);
        }

        public async Task AddAsync(TDanhMucSp danhMuc)
        {
            _context.TDanhMucSps.Add(danhMuc);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(TDanhMucSp danhMuc)
        {
            _context.Entry(danhMuc).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(string id)
        {
            var danhMuc = await _context.TDanhMucSps.FindAsync(id);
            if (danhMuc != null)
            {
                _context.TDanhMucSps.Remove(danhMuc);
                await _context.SaveChangesAsync();
            }
        }
    }
}
