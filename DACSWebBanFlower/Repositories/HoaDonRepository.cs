﻿using DACSWebBanFlower.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DACSWebBanFlower.Repositories
{
    public class HoaDonRepository : IHoaDonRepository
    {
        private readonly QlbanFlowerContext _context;

        public HoaDonRepository(QlbanFlowerContext context)
        {
            _context = context;
        }

        public async Task AddAsync(THoaDonBan hoaDon)
        {
            _context.THoaDonBans.Add(hoaDon);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<THoaDonBan>> GetAllAsync()
        {
            return await _context.THoaDonBans.ToListAsync();
        }

        public async Task<THoaDonBan> GetByIdAsync(string id)
        {
            return await _context.THoaDonBans.FindAsync(id);
        }

        public async Task RemoveAsync(string id)
        {
            var hoaDon = await GetByIdAsync(id);
            if (hoaDon != null)
            {
                _context.THoaDonBans.Remove(hoaDon);
                await _context.SaveChangesAsync();
            }
        }

        public async Task UpdateAsync(THoaDonBan hoaDon)
        {
            _context.Entry(hoaDon).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}

