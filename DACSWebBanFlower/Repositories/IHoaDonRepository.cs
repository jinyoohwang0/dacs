﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DACSWebBanFlower.Models;
using Microsoft.EntityFrameworkCore;
namespace DACSWebBanFlower.Repositories
{
    public interface IHoaDonRepository 
    {
        Task<THoaDonBan> GetByIdAsync(string id);
        Task<IEnumerable<THoaDonBan>> GetAllAsync();
        Task AddAsync(THoaDonBan hoaDon);
        Task UpdateAsync(THoaDonBan hoaDon);
        Task RemoveAsync(string id);
    }
}
