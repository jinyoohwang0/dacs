﻿using DACSWebBanFlower.Models;
using Microsoft.AspNetCore.Mvc;
using DACSWebBanFlower.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using DACSWebBanFlower.Models.ProductModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using DACSWebBanFlower.Repositories;

namespace DACSWebBanFlower.Controllers
{
    public class StoreController : Controller
    {
        private readonly QlbanFlowerContext _context;
        private readonly IDanhMucSpRepository _danhMucSpRepository;
        // Định nghĩa thuộc tính HttpContextAccessor để truy xuất HttpContext
        private readonly IHttpContextAccessor _httpContextAccessor;
        // Constructor để inject IHttpContextAccessor vào controller
        public StoreController(QlbanFlowerContext context,IDanhMucSpRepository danhMucSpRepository , IHttpContextAccessor httpContextAccessor)
        {
            _danhMucSpRepository = danhMucSpRepository;
            _httpContextAccessor = httpContextAccessor;
            _context = context;
        }

        public ShoppingCart Carts
        {
            get
            {
                var session = _httpContextAccessor.HttpContext.Session; // Lấy session từ HttpContextAccessor
                var cart = session.GetObjectFromJson<ShoppingCart>("GioHang"); // Sử dụng Extension Helpers để lấy dữ liệu từ session
                if (cart == null)
                {
                    cart = new ShoppingCart();
                }
                return cart;
            }   
            set
            {
                var session = _httpContextAccessor.HttpContext.Session; // Lấy session từ HttpContextAccessor
                session.SetObjectAsJson("GioHang", value); // Sử dụng Extension Helpers để lưu dữ liệu vào session
            }
        }

        public IActionResult AddToCart(string id, int SoLuong)
        {
            var myCart = Carts;
            var item = myCart.Items.SingleOrDefault(p=>p.MaSp==id);
            if (item == null)
            {
                var sanPham = _context.TDanhMucSps.SingleOrDefault(p => p.Id == id);
                item = new CartItem
                {
                    MaSp = id,
                    TenSp = sanPham.TenSp,
                    Price = sanPham.GiaNhoNhat, // Gán giá trị của thuộc tính Price của đối tượng sanPham vào thuộc tính Price của item
                    SoLuong = 1,
                };
                myCart.Items.Add(item);
            }
            else
            {   
                item.SoLuong++;
            }
            HttpContext.Session.SetObjectAsJson("GioHang", myCart);

            return RedirectToAction("Index");
        }
        //============================================================================
        //Cap nhap lai gio hang
        [HttpPost]
        public IActionResult UpdateCartItem(string id, int SoLuong)
        {
            // Kiểm tra dữ liệu đầu vào
            if (SoLuong <= 0)
            {
                // Xử lý khi số lượng không hợp lệ
                // Ví dụ: Hiển thị thông báo lỗi và redirect về trang giỏ hàng
                TempData["ErrorMessage"] = "Số lượng không hợp lệ.";
                return RedirectToAction("Index");
            }

            var myCart = Carts;
            var item = myCart.Items.SingleOrDefault(p => p.MaSp == id);

            // Kiểm tra sản phẩm có tồn tại không
            if (item == null)
            {
                // Xử lý khi sản phẩm không tồn tại trong giỏ hàng
                // Ví dụ: Hiển thị thông báo lỗi và redirect về trang giỏ hàng
                TempData["ErrorMessage"] = "Sản phẩm không tồn tại trong giỏ hàng.";
                return RedirectToAction("Index");
            }

            // Cập nhật số lượng của sản phẩm
            item.SoLuong = SoLuong;
            Carts = myCart;

            return RedirectToAction("Index");
        }

        // Xóa sản phẩm khỏi giỏ hàng
        [HttpPost]
        public IActionResult RemoveFromCart(string id)
        {
            var myCart = Carts;
            var item = myCart.Items.SingleOrDefault(p => p.MaSp == id);
            if (item != null)
            {
                myCart.Items.Remove(item);
                Carts = myCart;
            }
            return RedirectToAction("Index");
        }

        // Xóa toàn bộ sản phẩm khỏi giỏ hàng
        [HttpPost]
        public IActionResult ClearAll()
        {
            Carts.Items.Clear();
            HttpContext.Session.Remove("GioHang");
            return RedirectToAction("Index");
        }
        public IActionResult Index()
        {
            var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("GioHang") ?? new ShoppingCart();
            return View(cart); // Truyền giỏ hàng vào view
        }


        //================================================================================

        [HttpGet]
        public IActionResult ThanhToan()
        {
            return View(new THoaDonBan());
        }
        
        [HttpPost]
        public async Task<IActionResult> ThanhToan(THoaDonBan hoaDonBan)
        {
            // Kiểm tra xem session có tồn tại hay không
            if (HttpContext.Session.Keys.Contains("GioHang"))
            {
                // Session đã tồn tại
                var gioHang = HttpContext.Session.GetObjectFromJson<ShoppingCart>("GioHang");
                // Tiếp tục xử lý dựa trên giỏ hàng
            }
            else
            {
                // Session chưa tồn tại
                // Thực hiện các hành động phù hợp
            }
            var Carts = HttpContext.Session.GetObjectFromJson<ShoppingCart>("GioHang");
            if (Carts == null || !Carts.Items.Any())
            {
                // Xử lý giỏ hàng trống...
                return RedirectToAction("Index");
            }
            
            hoaDonBan.NgayHoaDon = DateTime.UtcNow;
            hoaDonBan.TongTienHd = Carts.Items.Sum(i => i.Price * i.ThanhTien);
            hoaDonBan.TChiTietHdbs = Carts.Items.Select(i => new TChiTietHdb
            {
                Id = i.MaSp,
                SoLuongBan = i.SoLuong,
                DonGiaBan = i.Price
            }).ToList();
            // Tiếp tục cung cấp giá trị cho các trường còn thiếu...
            try
            {
                //Luu đơn hàng và chi tiết đơn hàng 
                _context.THoaDonBans.Add(hoaDonBan);
                await _context.SaveChangesAsync();
                HttpContext.Session.Remove("GioHang");
                return View("OrderCompleted", hoaDonBan.Id); // Trang xác nhận hoàn thành đơn hàng
            }
            catch (Exception ex)
            {
                // Xử lý nếu có lỗi khi lưu đơn hàng
                TempData["ErrorMessage"] = "Có lỗi xảy ra khi xử lý đơn hàng. Vui lòng thử lại sau.";
                // Điều hướng người dùng về trang chính hoặc trang thanh toán
                return RedirectToAction("Index");
            }
        }


        // Thêm action OrderComplete để hiển thị trang xác nhận hoàn thành đơn hàng
        private async Task<TDanhMucSp> GetTDanhMucSpFromDatabaseAsync(string Id)
        {
            // Trả về view "OrderCompleted" và truyền orderId vào làm mô hình dữ liệu
            return await _danhMucSpRepository.GetByIdAsync(Id);
        }



    }
}
